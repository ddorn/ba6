# Courses notes MA-BA6

This repository holds all my notes from the classes I've followed in the spring
semester of 2021. They are mostly 3rd year and master classes.

The notes are all written in LaTeX, so you need a LaTeX compiler in order to
read them in pdf.

For each class, the notes are contained in the corresponding folder, except
for the preamble, which common to all of them and is located in the top-level
folder.


### Link to compiled versions of the notes:
 - [Algebraic topology](https://gitlab.com/ddorn/ba6/-/jobs/artifacts/master/raw/topo-alg/topo-alg.pdf?job=pdf)
 - [Bachelor Project](https://gitlab.com/ddorn/ba6/-/jobs/artifacts/master/raw/projet/projet.pdf?job=pdf)
