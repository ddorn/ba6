\input{../../preambule}

\title{Exercise 12}
\author{Diego Dorn}

\begin{document}
    \maketitle

    \subsection*{Question 1}
    In order to simplify the notations,
    we identify $X$ with $S^k \vee S^l$.
    Following this notation,
    let $A = S^n \setminus S^k$ and
    $B = S^n \setminus S^l$ be the topological spaces
    obtained by removing from the $n$-sphere a space
    homeomorphic to $S^k$ (respectively $S^l$).

    We have $A \cap B = S^n \setminus X$ and $A \cup B = S^n \setminus \set{x}$,
    where $x$ is the point where the wedge of the to spheres is done.
    Since $A \cup B \simeq \R^n$, we have that $\tilde H_i(A \cup B) = 0$
    for all $i$.
    We also know from the course that
    \[
        \tilde H_i(A) = \begin{cases}
            \Z & \tif i = n - k - 1 \\
            0 & \otherwise
        \end{cases}
        \quad
        \quad
        \quad
        \tilde H_i(B) = \begin{cases}
            \Z & \tif i = n - l - 1 \\
            0 & \otherwise.
        \end{cases}
    \]

    Using the Mayer Vietoris sequences on $A \cup B$, we have
    \begin{center}
        \begin{tikzcd}[row sep=0.5cm]
            && \dotsto
                \underbrace{
                    \tilde H_{i+1}(A \cup B)
                }_{=~0} \connecting{dll}{}
            \\
            \tilde H_{i}(A \cap B) \rar
                & \tilde H_i(A) \oplus \tilde H_i(B) \rar
                    & \underbrace{
                        \tilde H_i(A \cup B)
                      }_{=~0} \todots
        \end{tikzcd}
    \end{center}
    Which induces an isomorphism
    \[
        \tilde H_i(S^n \setminus X)
        = \tilde H_i(A \cap B)
        \simeq
            \tilde H_i(A) \oplus \tilde H_i(B).
    \]
    That is,
    \[
        \tilde H_i(S^n \setminus X) =
        \begin{cases}
            \Z^2 & \tif k = l \andd i = n - k - 1 \\
            \Z & \tif k \neq l \andd i = n - k - 1 \text{ or } i = n - l - 1 \\
            0 & \otherwise.
        \end{cases}
    \]


    \subsection*{Question 2}

    Similarly to the last question,
    we let $A = S^n \setminus S^k$ and
    $B = S^n \setminus S^l$ be the subspaces of $S^n$
    with a copy of $S^k$ and $S^l$ removed respectively.
    $A$ and $B$ have the same homology groups as the $A$ and $B$
    from the previous question, but this time
    $A \cap B = S^n \setminus X$
    and $A \cup B = S^n$ since the copies of $S^k$ and $S^l$ removed are disjoint.
    Therefore \[
        \tilde H_i(A \cup B) = \begin{cases}
            \Z & \tif i = n \\
            0 & \otherwise.
        \end{cases}
    \]
    So the only different with the exercice 1 is that
    $\tilde H_n(A \cup B)$ is \Z and not $0$.
    Using Mayer Vietoris, we obtain the following long exact sequence
    \begin{center}
        \begin{tikzcd}[row sep=0.5cm]
            & \dotsto \tilde H_{i+1}(A) \oplus \tilde H_{i+1}(B) \rar
                & \tilde H_{i+1}(A \cup B) \connecting{dll}{}
            \\
            \tilde H_{i}(A \cap B) \rar
                & \tilde H_i(A) \oplus \tilde H_i(B) \rar
                    & \tilde H_i(A \cup B) \todots
        \end{tikzcd}
    \end{center}

    Now if $i \neq n$ and $i + 1 \neq n$,
    then $\tilde H_i(A \cup B)$ and $\tilde H_{i+1}(A \cup B)$
    are both trivial, so we have an isomorphism
    \[
        \tilde H_i(S^n \setminus X)
        = \tilde H_i(A \cap B)
        \simeq
            \tilde H_i(A) \oplus \tilde H_i(B).
    \]
    That is, we obtain the same thing as in question 1,
    \[
        \tilde H_i(S^n \setminus X) \simeq
        \begin{cases}
            \Z^2 & \tif k = l \andd i = n - k - 1 \\
            \Z   & \tif k \neq l \andd i = n - k - 1 \text{ or } i = n - l - 1 \\
            0    & \otherwise.
        \end{cases}
    \]

    For the case $i = n$, we have that $\tilde H_n(S^n \setminus X) = 0$
    since $\tilde H_i(A) = \tilde H_i(B) = \tilde H_{i+1}(A \cup B) = 0$.

    For the case $i + 1 = n$, we notice than since $k, l \geq 1$,
    the homology groups  $\tilde H_{i}(A)$ and $\tilde H_{i}(B)$
    are both trivial so that we are left with
    following exact sequence:
    \[
        \underbrace{
            \tilde H_{n}(A) \oplus \tilde H_{n}(B)
        }_{=~0}
        \to
        \underbrace{
            \tilde H_{n}(A \cup B)
        }_{\simeq~\Z}
        \to
        \tilde H_{n - 1}(A \cap B)
        \to
        \underbrace{
            \tilde H_{n - 1}(A) \oplus \tilde H_{n - 1}(B)
        }_{=~0}
    \]
    So that
    \[
        \tilde H_{n - 1}(S^n \setminus X) = \Z.
    \]



    \subsection*{Question 3}
    \paragraph{a)} We define the function $f: S^{n-1} \to S^{n-1}$
    as \[
        f(x) = \frac{x \cdot x}{ \|x \cdot x\| }
    \]
    where we denote by $\cdot$ the multiplication on $\R^n$.
    The image is clearly in $S^{n-1}$ as $x \cdot x$ is never zero
    on non zero elements of a field.
    We also have that $f(-x) = f(x)$ since the multiplication is
    \R-linear and thus $(-x) \cdot (-x) = x \cdot x$.
    Finally, we need to show that the map induced $\bar{f} : \R P^n \to S^{n-1}$
    is injective, that is, for any $x, y \in S^{n-1}$, such that
    $f(x) = f(y)$, $x = \pm y$.
    We have
    \begin{align*}
        f(x) = f(y)
        ~\iff&~
        x^2 = \lambda y^2
        &\quad \text{with }\lambda > 0
        \\ \iff&~
        x^2 - \parenthesis{\sqrt{\lambda}y}^2 = 0
        &\\ \iff&~
        (x - \sqrt{\lambda}y)(x + \sqrt{\lambda}y) = 0
        \\ \iff&~
        x = \pm\sqrt{\lambda}y
    \end{align*}
    Therefore $\lambda$ must be one as both $x$ and $y$ are on the sphere,
    thus $x = \pm y$.

    \paragraph{b)}
    Since for $n \geq 1$, $\R P^{n}$ is compact and $S^{n}$ is Hausdorff,
    the map $f$ is an embedding. Furthermore, $\R P^{n}$ is connected,
    the embedding $f$ is surjective and thus an homeomorphism.
    However, for $n > 1$, $\R P^{n}$ is not homoemorphic to $S^{n}$.
    and thus there cannot be a field structure on $\R^{n+1}$, for $n + 1 > 2$.

    \subsection*{Question 4}
    \begin{wrapfigure}{r}{0.2\linewidth}
        \includegraphics[width=\linewidth]{hausdorff.png}
    \end{wrapfigure}
    \paragraph{Félix Hausdorff} (1868-1942) is a German mathematician considered as one of the fathers
    of topology and has also contributed greatly set theory, measure theory
    and the beginning of descriptive set theory.

    Initially, Félix Hausdorff also had a great musical talent
    and wanted to become a composer, but his father made him give up his plans
    and pursue instead a degree in science.
    While he was studying mathematics and astronomy in the city of Leipzig,
    he also attended lectures on physics, chemistry, geography, philosophy,
    language, literature and social sciences. He was very versatile.

    His thesis was about the absorbance of light on the atmosphere,
    directed by Heinrich Bruns,
    but never gained any momentum as its application required observations
    that cannot be obtained with the required accuracy.

    Before his PHD, Hausdorff completed a volunteer military service
    and worked for two years as a human computer at the observatory in Leipzig.
    When he became a professor at the university of Leipzig in 1901,
    he wasn't accepted by the whole recruitment comity, as 7 out 22 opposed,
    because he was Jew.
    He was one of the first to ever give a lecture on set theory,
    only after Ernst Zermelo who did one the year before.

    His works include a thorough study of ordered sets while trying to
    solve the continuum hypothesis. He introduced the notion of cofinality,
    central in set theory and raised the question of inaccessible cardinals.

    In his ``Principles of Set Theory'', he also developed
    the theory of `point sets' but also dimension and measure theory.
    He studied systematically the topological spaces based on the
    concept of open set, that he called ``area'', added the separation axioms,
    founded the theory of connected sets and developed the notion of compactness,
    introduced by Fréchet. He later introduced the term ``metric space''
    for which Fréchet used ``class (E)'' before.
    Later in the book, he also introduced a long list of new concepts, as completeness, total boundedness,
    reducible sets. He also give the first correct proof of the strong law of large numbers.

    He later introduced the notion of Hausdorff measure and dimension,
    which are at the core of fractal theory, dynamical systems and geometric measure theory.

    Aside from his mathematical activity, he is also the author of
    numerous essays on philosophy and a play that has been performed in more that third cities
    as well as a book of poems.

    On the 26th of January 1942, Félix Hausdorff and his wife commit suicide to
    avoid the Holocaust.
\end{document}
