\input{../../preambule}

\title{Exercice 5}
\author{Diego Dorn}

\begin{document}
    \maketitle

    \subsection*{Question 1}
    \paragraph{(a)}
    Let $r: X \to A$ be the retraction from $X$ to $A$.
    We know that $r$ induces a group morphism $r_*: H_n(X) \to H_n(A)$.
    However, $r_* \circ i_* = (r \circ i)_* = (\id_A)_* = \id_{H_n(A)}$.
    Therefore, $i_*$ must be injective, since the composition $r_*\circ i_*$ is injective.


    \paragraph{(b)}
    If $\d D^n$ is a retract of $D^n$, by the point (a), the inclusion
    induces an injective morphism $i_*: H_n(\d D^n) \to H_n(D^n)$.
    However, since $\d D^n = S^{n-1}$, we have that $H_n(\d D^n) = \Z$
    and $H_n(D^n) = 0$ since $D^n$ is contractible.
    Therefore, $i_*: \Z \to 0$ cannot be injective.

    \paragraph{(c)}
    Suppose that $f: D^n \to D^n$ has no fixed point.
    We define a continuous map $g: D^n \to \d D^n$ the following
    way: let $x \in D^n$, $g(x)$ is the projection of $f(x)$ 
    towards $x$ on the border of $D^n$. In other terms,
    $g(x) = x + \lambda (x - f(x))$, with $\lambda \geq 0$ and 
    $\|g(x)\| = 1$. This function is clearly continuous
    and is also a retraction, because when $x \in \d D^n$, 
    we always have $\lambda = 0$ and thus $g(x) = x$.

    Now we have a retraction from $D^n$ to $\d D^n$, 
    which was proven impossible by part (b), therefore
    $f$ must have a fixed point.

    \subsection*{Question 2}
    \paragraph{(a)}
    We can construct a \D-structure on the real projective
    plane with two 0-simplex, three 1-simplex and two 2-simplex.

    \begin{center}
        \begin{tikzpicture}[scale=2.5]
            \path
                coordinate (a) at (0,0)
                coordinate (b) at (0,1)
                coordinate (c) at (1,1)
                coordinate (d) at (1,0);  % Make macro ?

            \foreach \n/\label/\anchor in {a/x/below left,b/y/above left,c/x/above right,d/y/below right}
                \node[\anchor] at (\n) {$\label$};

            \foreach \from/\to/\name/\anchor in {a/b/a/left, c/d/a/right, 
                a/d/b/below, c/b/b/above, 
                a/c/c/right}
                \draw[marr=\Singlearrow] (\from) -- node[\anchor=2pt] {$\name$} (\to);

            \draw[<-] (0.15, 0.65) arc (220:-40:4pt);
            \node at (0.26, 0.73) {$u$};
            \draw[<-] (0.85, 0.12) arc (-40:220:4pt);
            \node at (0.75, 0.21) {$v$};
        \end{tikzpicture}
    \end{center}

    \paragraph{(b)}
    The Euler characteristic of the projective plane is then 
    $$
        \chi = 2 - 3 + 2 = 1
    $$

    \paragraph{(c)}
    We have the following chain complex:
    $$
        \dots \to
        0
        \xto{\d_3}
            \verteq{
                \D_2(X)
            }{
                \Z u \oplus \Z v
            }
        \xto{\d_2}
            \verteq{
                \D_1(X)
            }{
                \Z a \oplus \Z b \oplus \Z c
            }
        \xto{\d_1}
            \verteq{
                \D_0(X)
            }{
                \Z x \oplus \Z y
            }
        \xto{\d_0}
            0.
    $$
    And we compute the kernel and images of $\d_1$ and $\d_2$:
    \begin{itemize}
        \item $\d_1(a) = \d_1(b) = \d_1(c) = x - y$, so $\im \d_1 = \Z(x - y)$
        \item The kernel of $\d_1$ must have a rank of two, 
            thus $\ker \d_1 = \Z(a - b) \oplus \Z(a - c)$.
        \item $\d_2(u) = -a + b + c$ and $\d_2(v) = a - b + c$, hence 
            $$
                \im \d_2 = 
                    \Z(2c) 
                    \oplus
                    \Z(a - b + c).
            $$
        \item Finally, $\ker \d_2 = 0$
    \end{itemize} 

    All this let us find the homojoly groups:
    \begin{itemize}
        \item $H^\D_0 = \quotient{\Z x \oplus \Z y}{ \Z(x - y)} \simeq \Z$
        \item We have
        \begin{align*}
            H^\D_1 
            &=
                \quotient{\ker \d_1}{\im \d_2}
            \\&=
                \quotient{
                    \Z(a - b) \oplus \Z(a - c)
                }{
                    \Z(2c) 
                    \oplus
                    \Z(a - b + c)
                }
            \\&=
                \quotient{
                    \Z(c) \oplus \Z(a - c)
                }{
                    \Z(2c)
                }
            \\&\simeq
                \quotient{\Z}{2\Z} \oplus \Z.
        \end{align*}
        \item $H^\D_2 = \quotient{0}{0} = 0$ and the same is true for higher homotopy groups.
    \end{itemize}

    \subsection*{Question 3}

    Let $(A_n)$ and $(B_n)$ be two chain complexes 
    and $f, g, h$ be three chain maps from $A$ to $B$.
    
    First, to show that the chain homotopy is a reflexive
    relation, we need to find $P$ such that $\d P + P\d = f - f = 0$.
    Taking $P_n = 0$ is thus sufficient.

    Then, we show that it is a symetric relation. Suppose
    that $P$ is a chain homotopy between $f$ and $g$, 
    then $-P$ is also a chain homotopy and 
    $$
        \d P + P\d = g - f
        \iff 
        -\d P - P\d = f - g
        \iff 
        \d \circ (-P) + (-P)\d = f - g.
    $$

    Finally, we show that it is transitive. 
    Let $P$ be a chain homotopy between $f$ and $g$, such that
    $$\d P + P\d = g - f$$
    and $Q$ be a chain homotopy between $g$ and $h$, such that
    $$\d Q + Q\d = h - g.$$

    Then we have 
    \begin{align*}
        h - f
        &= 
        h - g + g - f
        \\&=
        \d P + P\d + \d Q + Q\d
        \\&=
        \d(P+Q) + (P+Q)\d,
    \end{align*}
    since all maps involved are morphisms. 
    Therefore, the chain homotopy $P+Q$ is a chain homotopy
    between $f$ and $h$.





\end{document}