LATEX = latexmk

.PHONY: all topo-alg projet clean distclean

all: topo-alg projet

topo-alg: topo-alg/topo-alg.pdf
projet: projet/projet.pdf

%.pdf: %.tex preambule.tex
	# @echo "*: $*"
	# @echo "@: $@"
	# @echo "<: $<"
	$(LATEX) -cd -pdf -quiet $<


clean:
	@for d in */*.tex *.tex; do latexmk -quiet -cd $$d -c &&  echo "cleaned aux for '$$d'"; done;

distclean:
	@for d in */*.tex *.tex; do latexmk -quiet -cd $$d -CA &&  echo "cleaned all generated files for '$$d'"; done;

