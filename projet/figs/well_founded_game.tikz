\begin{figure}
    \centering
    \begin{tikzpicture}[scale=1.5]
        \foreach \x in {0, 1, 2, 3}{
            \fill[myGreen, rounded corners=10pt] (2*\x - 0.25, 0.3) rectangle (2*\x + 1.25, -5);
            \node at (2*\x, 0) {\I};
            \node at (2*\x + 1, 0) {\II};
            \node at (2*\x + 1.5, 0.2) {$A_{\x}^{\e(\x)}$};
            \node at (2*\x + 0.5, 0.5) {$G_\x$};
        };
        \node at (-0.4, 0.2) {$A$}; 
        \node at (8, 0) {\dots};

        % First column (x_0)
        \foreach \y in {0, ..., 3} {
            \node (I0\y) at (0, -\y - 0.5) {$x_\y^0$};
        };
        % moves + arrows between games
        \foreach \x in {1, ..., 3}{
            \pgfmathsetmacro{\z}{3-\x}
            \foreach \y in {0, ..., \z} {
                \node (I\x\y) at (2*\x, -\y - 0.5) {$x_\y^\x$};
                \node (II\x\y) at (2*\x - 1, -\y-1) {$x_\y^\x$};
                \draw[->, dashed] (I\x\y) -- (II\x\y);

                \pgfmathsetmacro{\xprev}{int(\x-1)}
                \pgfmathsetmacro{\ynext}{int(\y+1)}
                \draw[->] (II\x\y) 
                    -- node[above, sloped] {$\tau_\xprev$}
                    (I\xprev\ynext);
            };
        };

        \foreach \x in {0, 1, 2, 3} {
            % \node at (2*\x, -4+\x) {\vdots};
            % \node at (2*\x + 1, -4+\x+0.5) {\vdots};
            \draw[line cap=round, dash pattern=on 0 off 1cm/4, very thick] 
                (2*\x, -4+\x) -- (2*\x, -4.49)
                (2*\x + 1, -3.5+\x) -- (2*\x + 1, -4.49);
        };

        \draw (-0.5, -4.5) -- (7.5, -4.5);
        \foreach \x in {0, 1, 2} {
            \node at (2*\x + 1, -4.75) {$x^\x$};
            \node at (2*\x + 2, -4.75) {$x^\x$};
        };
        \node at (0, -4.75) {$x^{\phantom{0}}$};
        \node at (7, -4.75) {$x^4$};

        \node at (7.6, -4.75) {\dots};

    \end{tikzpicture}
    \caption{
        The composition of the sequence of games.
    }
    \label{fig:well-founded-game}
\end{figure}