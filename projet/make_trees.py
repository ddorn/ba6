from dataclasses import dataclass
from glob import glob
from pathlib import Path
from textwrap import dedent, indent
from typing import List, Optional, Tuple

INDENT = " " * 4

FIGS = Path(__file__).parent / "figs"


def parse_from_indent(s) -> List[str]:
    s = dedent(s)

    parts = []

    lines = []
    for line in s.splitlines():
        if not line.startswith(" ") and lines:
            parts.append(dedent("\n".join(lines)))
            lines.clear()

        if line.strip():
            lines.append(line)

    if lines:
        parts.append(dedent("\n".join(lines)))

    return parts

class Tree:
    def __init__(self, value, *children: "Tree", depth, domain=""):
        self.value: str = value
        self.domain: str = domain
        self.children: Tuple[Tree] = children
        self.depth = depth

    def __str__(self):
        # if self.depth <= 0:
        #     return ""

        children_str = [str(c) for c in self.children if str(c)]

        s = ""
        for i, child in enumerate(children_str):
            c = str(child)
            if not c:
                continue
            if i == len(children_str) - 1:
                c = indent(c, INDENT)
                c = "└───" + c[4:]
            else:
                c = indent(c, "│   ")
                c = "├── " + c[4:] + "\n"
            s += c

        if s:
            s = "\n" + s

        return f"{self.value}    !{self.depth}{s}"

    @property
    def max_depth(self):
        return max(0, self.depth, *(c.max_depth for c in self.children))

    @classmethod
    def parse_node(cls, node, current_depth):
        node, _, domain = node.partition("&")

        stripped = node.lstrip("!")
        current_depth += len(node) - len(stripped)

        value = stripped.rstrip("!")
        next_depth = current_depth + len(stripped) - len(value)

        return value, domain, current_depth, next_depth

    @classmethod
    def from_seq_string(cls, s, depth=0):
        root, _, rest = s.strip().partition(" ")
        value, domain, depth, next_depth = cls.parse_node(root, depth)
        children = [cls.from_seq_string(rest, next_depth)] if rest else []
        return cls(value, *children, depth=depth, domain=domain)

    @classmethod
    def from_string(cls, s, depth=0):
        root, _, s = s.strip().partition("\n")
        children = []

        value, domain, depth, next_depth = cls.parse_node(root, depth)

        for part in parse_from_indent(s):
            c = cls.from_string(part, next_depth)
            children.append(c)

        return cls(value, *children, depth=depth, domain=domain)

    def to_tikz(self, depth=9999) -> str:
        if self.depth >= depth:
            return ""

        children_tikz = "\n".join(filter(None, map(Tree.to_tikz, self.children)))

        if children_tikz:
            children_tikz = f"\n{indent(children_tikz, INDENT)}\n"

        return "child { node {%s} %s}" % (self.value, children_tikz)

    def to_forest(self, depth=9999):
        if self.depth > depth:
            return f"[,phantom]"

        if self.depth == depth and self.depth != 0:
            style = ",edge={line width=1pt}"
            # style = ",edge={dotted}"
        else:
            style = ""

        children = list(filter(None, (n.to_forest(depth) for n in self.children)))

        s = "\n".join(children)

        if len(children) > 1 or (style and children) or "\n" in s or len(s) > 40:
            s = "\n" + s + "\n"
            s = indent(s, INDENT)

        if self.domain:
            value = "${\mathbf{%s}}_{%s}$" % (self.value, self.domain)
        else:
            value = self.value

        return f"[{value}{style}{s}]"


@dataclass
class Raw:
    """
    Base class for exporting raw files to forests/tikz.

    Availaible options:
        caption

    Shortucts can be defined in the header with
        @short=long text
    and then reused in the text by writing @short.
    """

    KIND = ""

    options: dict
    path: Path

    @staticmethod
    def preprocess(s: str):
        """Parse a raw string to remove the comments and replace the shortcuts."""

        # Remove comments, ie. lines starting with %
        new_s = ''
        for line in s.splitlines(True):
            if line.lstrip().startswith("%"):
                pass
            else:
                new_s += line

        # Apply shortcuts
        head, *_ = parse_from_indent(s)
        kind, _, rest = head.partition("\n")
        options = {}
        for line in dedent(rest).splitlines():
            if line.startswith('@'):
                short, _, long = line.partition('=')
                new_s = new_s.replace(short, long)
            elif "=" in line:
                opt, _, value = line.partition('=')
                options[opt] = value

        _, *bodies = parse_from_indent(new_s)
        return kind, options, bodies

    @property
    def depth(self):
        return 0

    @staticmethod
    def load(path: Path) -> "Raw":
        kind, *_= path.read_text().partition("\n")

        for cls in Raw.__subclasses__():
            if cls.KIND == kind:
                return cls.load(path)

        raise ValueError("No loader for type:", kind)

    def surrond_with_figure(self, s, depth):
        s = dedent(s).strip()
        label = r"\label{%s}" % f"fig:{self.get_name(depth, False)}"


        if 'caption' in self.options:
            label = r"\caption{%s}" % self.options['caption'] + "\n" + label

        out = r"""
\begin{figure}[h!]
\begin{center}
%s
\end{center}
%s
\end{figure}
            """ % (indent(s, INDENT), label)

        return dedent(out).strip()

    def export(self, hide_depth=9999):
        raise NotImplementedError()

    def export_all(self):
        for depth in range(self.depth + 1):
            yield self.export(depth)

    @staticmethod
    def convert(path: Path):
        obj = Raw.load(path)

        for depth, out in enumerate(obj.export_all()):
            out_path = FIGS / obj.get_name(depth)
            out_path.write_text(out)
            print(f"{out_path} written.")

    def get_name(self, depth, with_extension=True):
        if self.depth > 0:
            out = f"{self.path.stem}_{depth}"
        else:
            out = self.path.stem

        if with_extension:
            return out + ".tikz"
        return out


@dataclass
class RawGame(Raw):
    KIND = "GAME"

    alice: Tree
    bob: Tree
    alice_name: str
    bob_name: str

    @property
    def depth(self):
        return max(self.alice.max_depth, self.bob.max_depth) + 1

    @staticmethod
    def load(path: Path):
        kind, options, (alice, bob) = RawGame.preprocess(path.read_text())

        alice_name, _, alice = alice.partition("\n")
        bob_name, _, bob = bob.partition("\n")

        alice = Tree.from_seq_string(alice)
        bob = Tree.from_string(bob)

        return RawGame(options, path, alice, bob, alice_name, bob_name)

    def export(self, hide_depth=9999):
        template = r"""
\begin{forest} baseline
    [%s,for children={no edge}
ALICE
    ]
\end{forest}
\hspace{2cm}
\begin{forest} baseline
    [%s,for children={no edge}
BOB
    ]
\end{forest}
""" % (self.alice_name, self.bob_name)

        alice = self.alice.to_forest(hide_depth)
        bob = self.bob.to_forest(hide_depth)

        template = template.replace("ALICE", indent(alice, INDENT * 2))
        result = template.replace("BOB", indent(bob, INDENT * 2))

        return self.surrond_with_figure(result, hide_depth)


@dataclass
class RawTree(Raw):
    KIND = "TREE"

    tree: Tree

    @property
    def depth(self):
        return self.tree.depth

    @staticmethod
    def load(path: Path) -> "RawTree":
        kind, options, (tree,) = RawTree.preprocess(path.read_text())
        return RawTree(options, path, Tree.from_string(tree))

    def export(self, hide_depth=9999):
        base = r"""
\begin{forest} baseline
%s
\end{forest}
        """ % indent(self.tree.to_forest(hide_depth), INDENT)

        return self.surrond_with_figure(base, hide_depth)


# Not used
def game_to_tikz(alice: Tree, bob: Tree, hide_depth=9999):
    template = r"""
\begin{tikzpicture}[level distance=1.0cm]
    \node[->] (alice) 
        at (0, 0) 
        {\e}
ALICE;

    \node[->] (bob)
        at (5, 0) 
BOB;

    \node[above of=alice] {Player I};
    \node[above of=bob] {Player II};

\end{tikzpicture}"""

    bob_tree = "{%s}\n" % bob.value + "\n".join(n.to_tikz() for n in bob.children)

    template = template.replace(
        "ALICE", indent(alice.to_tikz(hide_depth), 2 * INDENT)
    )
    result = template.replace("BOB", indent(bob_tree, 2 * INDENT))

    return result


def main():
    for filename in (FIGS / "raw").glob("*.raw"):
        Raw.convert(filename)


if __name__ == "__main__":
    main()
